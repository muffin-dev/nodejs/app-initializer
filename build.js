const Path = require('path');
const NodeHelpers = require('@muffin-dev/node-helpers');

(async () => {

  await NodeHelpers.removeDirectory('./bin', true);
  await NodeHelpers.removeDirectory('./doc', true);
  await NodeHelpers.removeDirectory('./lib', true);

  try {
    await NodeHelpers.copyDirectory('./src/doc', './doc');
  }
  catch { }

  // Remove /test-project test directory
  await NodeHelpers.removeDirectory('./test-project', true);

  // If the /src/doc directory is missing or is empty, copy the main README.md file into /doc
  const docFiles = await NodeHelpers.readdirAsync('./src/doc', false, true, null, true, true);
  if (docFiles.length === 0) {
    await NodeHelpers.copyFileAsync('./README.md', Path.join('./doc', 'README.md'));
  }

})();