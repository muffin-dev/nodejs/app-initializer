# App-initializer-demo

Your project have successfully been created using the [Muffin Dev's App Initializer](https://www.npmjs.com/package/@muffin-dev/app-initializer) utility. This tool provides a configuration template and some utilities described below.

## Build process

Your project (event if it's an app or a package) can be built using `npm run build` script. This script is declared in your brand new project's `package.json` file.

This script will run the `build.js` file, which is a utility that does several things:

- Remove the previously built project files (for example, for an app, it will remove the `/dist` directory)
- Copy all files (except for `*.ts` which are transpiled later) into their appropriate directory (for example, for an app, it will copy the `/src` directory into a `/dist` directory)
- Run Typescript if you use it

## Run the app

If the generated project is an app, you can use `npm run start` script to run it.

Don't forget to build it before, using `npm run build`.

### Build and run

A special command has been added which is `npm run dev-start`. It's a shortcut for building the app, then running it.