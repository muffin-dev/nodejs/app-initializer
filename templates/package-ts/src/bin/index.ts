#!/usr/bin/env node
import { Command } from 'commander';
import test from '../lib';

const program = new Command();

program
  .name('#package-name#')
  .usage('')
  .addHelpText('beforeAll', 'For now, it just says hello to the World :)')
  .action(() => {
    test();
  });
