#!/usr/bin/env node
const { Command } = require('commander');
const test = require('../lib/index');

const program = new Command();

program
  .name('#package-name#')
  .usage('')
  .addHelpText('beforeAll', 'For now, it just says hello to the World :)')
  .action(() => {
    test();
  });
