const Path = require('path');
const NodeHelpers = require('@muffin-dev/node-helpers');

////////////////////////////////////////
// Settings
////////////////////////////////////////

// Defines the directory that contains all your app source files.
const SRC_DIR = Path.join(__dirname, './src');

// Defines the directory where the app is built.
const OUT_DIR = Path.join(__dirname, './dist');

(async () => {

  ////////////////////////////////////////
  // Remove existing build
  ////////////////////////////////////////

  await NodeHelpers.removeDirectory('./dist', true);

  ////////////////////////////////////////
  // Copy all static files
  ////////////////////////////////////////

  // Get all files (except for *.ts files)
  const files = await NodeHelpers.readdirAsync(SRC_DIR, false, true, 'ts', true, true, true);
  // Copy all these files to the output directory
  const copyProcesses = [];
  files.forEach(file => {
    const relPath = file.path.slice(SRC_DIR.length);
    copyProcesses.push(NodeHelpers.copyFileAsync(file.path, Path.join(OUT_DIR, relPath)));
  });
  await Promise.all(copyProcesses);

})();
