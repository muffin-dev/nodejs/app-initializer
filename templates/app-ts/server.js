const express = require('express');

////////////////////////////////////////
// Settings
////////////////////////////////////////

// Defines the port to use if process.end.PORT is not defined
const DEFAUTL_PORT = 3000;

////////////////////////////////////////
// Server
////////////////////////////////////////

const PORT = process.env.PORT || DEFAUTL_PORT;

const app = express();
app.get('/', (req, res) => {
  res.json({ message: 'Hello World!' });
});

app.listen(PORT, () => {
  console.log(`App started on port ${PORT}`);
});