# Muffin Dev for Node - Init, the project initializer

*Init* is a tool for generating Node projects (apps or packages) with the configurations used on several *Muffin Dev* projects.

It also setups configurations for several use cases:

- Prepare an app or web worker project for [Heroku](https://heroku.com) deployment
- Prepare an app project for [Surge](https://surge.sh) deployment
- Prepare a package for browser usage with [`rollup`](https://www.npmjs.com/package/rollup)
- Add an executable for a package (and provide CLI tools by installing [`commander`](https://www.npmjs.com/package/commander))
- Setup [`Typescript`](https://www.typescriptlang.org) or let you develop with vanilla JS
- Setup [`ES Lint`](https://eslint.org)
- Initialize a Git repository

Note that this tool is perfect for *my* development process. But feel free to contact me for any additional operations that can suits your needs! ;)

## Installation & user guide

You can use it without installing the module using [`npx`](https://www.npmjs.com/package/npx):

```bash
npx @muffin-dev/init
```

Or you can install it globally using npm, then run it with the `init` command:

```bash
npm i -g @muffin-dev/init
init
```

And then... just answer the questions!

### Options

You can also pass the project path to the `@muffin-dev/init` command directly, so it will skip the first question:

```bash
npx @muffin-dev/init /path/to/project
```