#!/usr/bin/env node
import { join, isAbsolute, basename } from 'path';
import { Command } from 'commander';
import validatePackageName from 'validate-npm-package-name';
import inquirer from 'inquirer';
import generateProject, { IProjectSettings } from '../lib';

const program = new Command();

function asAbsolute(path: string) {
  if (!path || path.trim().length === 0) { path = './'; }
  if (!isAbsolute(path)) {
    path = join(process.cwd(), path);
  }
  return path;
}

program
  .name('@muffin-dev/app-initializer')
  .usage('[projectPath]')
  .addHelpText('beforeAll', 'Shows a wizard to help you generate and initialize a Node.js project (apps or packages) based on templates.')
  .arguments('[projectPath]')
  .description('', {
    projectPath: 'The path to the directory in which you want to initialize a new project. If omitted, the wizard will ask you the path to that directory.'
  })
  .option('-s, --no-install', 'skip the installation of dependencies after the project is generated')
  .action(async (projectPath: string) => {
    const answers: Record<string, any> = { };
    answers.projectPath = projectPath || null;
    answers.noInstall = !program.opts().install;

    // Ask for path only if it's not already set
    if (!answers.projectPath || answers.projectPath.trim().length === 0) {
      Object.assign(answers, await inquirer.prompt([
        {
          type: 'input',
          name: 'projectPath',
          message: 'Select your project directory:',
          default: './',
          transformer: (answer) => asAbsolute(answer)
        }
      ]));
    }

    Object.assign(answers, await inquirer.prompt([
      {
        type: 'list',
        name: 'projectType',
        message: 'What kind of project do you want to create?',
        choices: [ 'app', 'package' ]
      },
      {
        type: 'input',
        name: 'projectName',
        message: 'How do you want to name your project?',
        validate: (answer) => {
          const validation = validatePackageName(answer);
          if (!validation || validation.errors) {
            let err = validation.errors[0];
            err = err.charAt(0).toUpperCase() + err.slice(1);
            err += ' (see https://www.npmjs.com/package/validate-npm-package-name#naming-rules)';
            return err;
          }
          return true;
        },
        default: basename(answers.projectPath)
      },
      {
        type: 'input',
        name: 'description',
        message: 'Project description (optional):',
        default: null
      },
      {
        type: 'input',
        name: 'author',
        message: 'Name the author (optional):',
        default: null
      },
      {
        type: 'input',
        name: 'keywords',
        message: 'Project keywords (separated by commas, optional):',
        default: null
      },
      {
        type: 'confirm',
        name: 'isPublic',
        message: 'Is this project public (so it can be accessed by other users from npm)?',
        default: false
      },
      {
        type: 'input',
        name: 'git',
        message: 'Git repository URL (optional):',
        default: null
      }
    ]));

    if (answers.projectType === 'app') {
      Object.assign(answers, await inquirer.prompt([
        {
          type: 'list',
          name: 'appSettings.deployPlatform',
          message: 'What platform your app should be deployed on?',
          choices: [ 'Heroku', 'Surge', 'I\'ll setup the deployment platform myself' ],
          default: null
        }
      ]));
      if (answers.appSettings.deployPlatform) {
        answers.appSettings.deployPlatform.trim().toLowerCase();
      }

      if (answers.appSettings.deployPlatform === 'heroku') {
        Object.assign(answers, await inquirer.prompt([
          {
            type: 'confirm',
            name: 'appSettings.isWorker',
            message: 'Is your Heroku app a worker?',
            default: false
          }
        ]));
      }

      else if (answers.appSettings.deployPlatform === 'surge') {
        Object.assign(answers, await inquirer.prompt([
          {
            type: 'input',
            name: 'appSettings.surgeAppDomain',
            message: 'Your Surge app domain (optional, e.g. my-app.surge.sh):',
            default: null
          }
        ]));
      }
    }

    if (answers.projectType === 'package') {
      Object.assign(answers, await inquirer.prompt([
        {
          type: 'confirm',
          name: 'packageSettings.hasBin',
          message: 'Does your package includes a bin executable?',
          default: false
        },
        {
          type: 'confirm',
          name: 'packageSettings.useInBrowser',
          message: 'Is your package a library that can be used in browsers?',
          default: false
        }
      ]));
    }

    Object.assign(answers, await inquirer.prompt([
      {
        type: 'confirm',
        name: 'useTypescript',
        message: 'Do your want to use Typescript?',
        default: true
      },
      {
        type: 'confirm',
        name: 'useESLint',
        message: 'Do your want to use ESLint?',
        default: true
      }
    ]));

    Object.assign(answers, await inquirer.prompt([
      {
        type: 'list',
        name: 'testFramework',
        message: 'What platform your app should be deployed on?',
        choices: [ 'Mocha', 'I\'ll setup the test framework myself' ],
        default: 'mocha'
      }
    ]));
    if (answers.testFramework) {
      answers.testFramework.trim().toLowerCase();
    }

    console.log('');
    await generateProject(answers as IProjectSettings);
  });

program.parse(process.argv);
