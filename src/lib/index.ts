import { join } from 'path';
import { overrideObject } from '@muffin-dev/js-helpers/lib/object';
import { IProjectSettings } from './interfaces';
import { DEFAULT_PROJECT_SETTINGS } from './consts';
import validatePackageName from 'validate-npm-package-name';
import { isNullOrEmpty } from './utilities';
import { Generator } from './generator';
import { asAbsolute } from '@muffin-dev/node-helpers';

/**
 * Generates a project based on the given settings.
 * @param settings The settings of the project to generate.
 */
export async function generateProject(settings: IProjectSettings): Promise<void> {
  // Apply default values
  settings = overrideObject(Object.assign({ }, DEFAULT_PROJECT_SETTINGS), settings) as IProjectSettings;

  // Verify project type
  if (settings.projectType !== 'app' && settings.projectType !== 'package') {
    return console.error('Invalid project type');
  }

  // Verify project name
  if (isNullOrEmpty(settings.projectName) || !validatePackageName(settings.projectName)) {
    return console.error('Invalid project name. The name of your project must matches the npm package naming rules: https://www.npmjs.com/package/validate-npm-package-name#naming-rules');
  }

  // Process project path
  if (isNullOrEmpty(settings.projectPath)) { settings.projectPath = './'; }
  settings.projectPath = asAbsolute(settings.projectPath);

  const generator = new Generator(settings, join(__dirname, '../templates'));
  require('./generators')(generator);
  await generator.use();

  console.log('Project generated at ' + settings.projectPath);
}

export * from './interfaces';
export default generateProject;
