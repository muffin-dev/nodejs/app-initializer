import * as ChildProcess from 'child_process';
import * as NodeHelpers from '@muffin-dev/node-helpers';

/**
 * Executes the given command synchronously.
 * @async
 * @param command The command to execute.
 * @param path If given, defines the working directory of the created child process.
 * Resolves the promise with the console buffer parsed as string.
 */
export function execSync(command: string, path: string = null): Promise<string> {
  return new Promise((resolve: (data: string) => void, reject: (reason: unknown) => void) => {
    try {
      const result = ChildProcess.execSync(command, { cwd: path || null });
      resolve(result.toString('utf8'));
    }
    catch (error) {
      reject(error ? error.message || error : error);
    }
  });
}

/**
 * Utility method to update a JSON file.
 * @async
 * @param filePath The relative path to the file to update, from the target project path.
 * @param modifier The method to call that will modify the file content when opened.
 */
export async function updateJsonFile(
  filePath: string,
  modifier: (content: any) => Promise<void> | void
): Promise<void> {
  let jsonContent: any = null;
  try {
    jsonContent = await NodeHelpers.readJSONFileAsync(filePath);
  }
  catch { jsonContent = { }; }
  await modifier(jsonContent);
  await NodeHelpers.writeFileAsync(filePath, JSON.stringify(jsonContent, null, 2));
}

/**
 * Utility method to update a text file.
 * @async
 * @param filePath The relative path to the file to update, from the target project path.
 * @param modifier The method to call that will modify the file content when opened.
 */
export async function updateFile(
  filePath: string,
  modifier: (content: string) => Promise<string> | string
): Promise<void> {
  let content: string = null;
  try {
    content = await NodeHelpers.readFileAsync(filePath) as string;
  }
  catch { content = ''; }
  content = await modifier(content);
  await NodeHelpers.writeFileAsync(filePath, content);
}

/**
 * Checks if the string is null, not a string, or an empty one.
 * @param str The string you want to validate.
 */
export function isNullOrEmpty(str: unknown): boolean {
  if (!str) return true;
  if (typeof str !== 'string') return true;
  if (str.trim().length === 0) return true;

  return false;
}
