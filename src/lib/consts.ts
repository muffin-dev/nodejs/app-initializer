import { IProjectSettings } from './interfaces';

export const DEFAULT_PROJECT_SETTINGS: IProjectSettings = {
  projectName: null,
  projectPath: null,
  projectType: 'app',
  author: null,
  description: null,
  keywords: [],
  git: null,
  isPublic: false,
  useTypescript: true,
  useESLint: true,
  testFramework: 'mocha',
  appSettings: {
    deployPlatform: 'heroku',
    surgeAppDomain: null
  },
  packageSettings: {
    hasBin: true,
    useInBrowser: false
  },
  noInstall: false
};
