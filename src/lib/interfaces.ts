/**
 * @interface IPackageProjectSettings Defines the user settings for a package project.
 */
export interface IPackageProjectSettings {
  /**
   * @property If enabled, creates a /bin directory and a "bin" key in package.json. The initializer will also install commander.
   */
  hasBin?: boolean;

  /**
   * @property If enabled, install Rollup module as dev dependency, in order to pack modules for browser usage.
   */
  useInBrowser?: boolean;
}

/**
 *  @interface IAppProjectSettings Defines the user settings for an app project.
 */
export interface IAppProjectSettings {
  /**
   * @property Defines the platform to which you want the app to be deployed.
   */
  deployPlatform?: 'heroku' | 'surge' | null;

  /**
   * @property Defines if the aapp to generate is a worker. Only set if deployPlatform is "heroku".
   */
  isWorker?: boolean;

  /**
   * @property Defines the app domain for surge deployment. Only set if deployPlatform is "surge".
   */
  surgeAppDomain?: string;
}

/**
 * @interface IProjectSettings Defines the user settings for a project to generate.
 */
export interface IProjectSettings {
  /**
   * @property Defines the path to the project directory.
   */
  projectPath: string;

  /**
   * @property Defines the type of the project to generate.
   */
  projectType: 'app' | 'package';

  /**
   * @property Defines the name of the project, as written for the "name" key in the package.json file.
   */
  projectName: string;

  /**
   * @property Defines the name or the author of the project, as written for the "author" key in the package.json file.
   */
  author?: string;

  /**
   * @property Sets the description of the project, as written for the "description" key in the package.json file.
   */
  description?: string;

  /**
   * @property Defines the keyword(s) of the project, as written for the "keywords" key in the package.json file.
   */
  keywords?: string[];

  /**
   * @property Defines if the project is public or not.
   */
  isPublic: boolean;

  /**
   * @property Defines the Git repository URL of this project, as written for the "repository" key in the package.json file. If defined,
   * this value is also use for the "home" and "bugs" keys in the package.json file.
   */
  git?: string;

  /**
   * @property The settings of a package project. Only used if the package type is "package".
   */
  packageSettings?: IPackageProjectSettings;

  /**
   * @property The settings of an app project. Only used if the package type is "app".
   */
  appSettings?: IAppProjectSettings;

  /**
   * @property Defines if this project uses Typescript.
   */
  useTypescript?: boolean;

  /**
   * @property Defines if this project uses ESLint.
   */
  useESLint?: boolean;

  /**
   * @property Defines the test framework to use.
   */
  testFramework?: 'mocha' | null;

  /**
   * @property Defines if user wants to install dependencies after the project has been generated or not.
   */
  noInstall?: boolean;
}
