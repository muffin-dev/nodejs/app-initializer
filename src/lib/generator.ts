import { asBoolean } from '@muffin-dev/js-helpers';
import cliProgress from 'cli-progress';
import { basename } from 'path';
import { IProjectSettings } from './interfaces';
import { execSync, isNullOrEmpty } from './utilities';

export type TGeneratorProcessorMethod = (generator: Generator) => Promise<void>;

/**
 * @class Utility class for project generation.
 */
export class Generator {

  private _settings: IProjectSettings = null;
  private _packages = new Map<string, boolean>();
  private _templatesDirectory: string = null;

  private _preProcessors = new Map<string, TGeneratorProcessorMethod>();
  private _postProcessors = new Map<string, TGeneratorProcessorMethod>();

  /**
   * Gets the path to the directory of the project to generate.
   */
  public get outputPath(): string {
    return this._settings.projectPath;
  }

  /**
   * Gets the path to the templates directory.
   */
  public get templatesDirectory(): string {
    return this._templatesDirectory;
  }

  /**
   * Gets user defined settings for project generation.
   */
  public get settings(): IProjectSettings {
    return this._settings;
  }

  /**
   * Gets the name of the package, without the scope part.
   */
  public get packageName(): string {
    const output = basename(this.settings.projectName.replace(/@/gim, ''));
    return output;
  }

  /**
   * Gets the name of the package, without the scope part, with first letter uppercase and without any space or underscore.
   */
  public get nicifiedPackageName(): string {
    let str = this.packageName;
    const matches = [ ...str.matchAll(/[\s\\_-]+/gim) ];
    for (let i = matches.length - 1; i >= 0; i--) {
      if (matches[i].index === str.length - 1) {
        str = str.slice(0, matches[i].index);
      }
      else {
        const left = str.slice(0, matches[i].index);
        let right = str.slice(matches[i].index + 1);
        right = right.charAt(0).toUpperCase() + right.slice(1);
        str = left + right;
      }
    }
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  /**
   * Class constructor.
   * @param settings The user defined settings for the project to generate.
   * @param templatesDirectory The path to the templates directory.
   */
  constructor(settings: IProjectSettings, templatesDirectory: string) {
    this._settings = settings;
    this._templatesDirectory = templatesDirectory;
  }

  /**
   * Registers a method to call on project preprocessing (before installing dependencies).
   * @param name The name of the operation (for debug and feedback purposes).
   * @param callback The method to call when pre-processing the project generation.
   */
  public registerPreProcessor(name: string, callback: TGeneratorProcessorMethod): void {
    this._preProcessors.set(name, callback);
  }

  /**
   * Registers a method to call on project postprocessing (after installing dependencies).
   * @param name The name of the operation (for debug and feedback purposes).
   * @param callback The method to call when post-processing the project generation.
   */
  public registerPostProcessor(name: string, callback: TGeneratorProcessorMethod): void {
    this._postProcessors.set(name, callback);
  }

  /**
   * Adds a package to install as dependency of the generated project.
   * @param packageName The name of the package to install.
   * @param isDevDependency Is this package a dev dependency?
   */
  public addDependency(packageName: string, isDevDependency = false): void {
    if (isNullOrEmpty(packageName)) {
      return;
    }
    this._packages.set(packageName, asBoolean(isDevDependency));
  }

  /**
   * Genrates the project.
   */
  public async use(): Promise<boolean> {
    this._settings.noInstall = asBoolean(this._settings.noInstall);

    // const bar = new cliProgress.SingleBar({
    //   format: '[{bar}] {operation}',
    //   barsize: 20
    // }, cliProgress.Presets.shades_classic);
    // let nbTasks = this._preProcessors.size + this._postProcessors.size;
    // if (!this._settings.noInstall) {
    //   nbTasks += 2;
    // }
    // bar.start(nbTasks, 0, { operation: '' });

    console.log('Generating project...');
    for (const [ name, processor ] of this._preProcessors) {
      try {
        // this._updateProgressBarOperation(bar, name);
        await processor(this);
        // bar.increment();
      }
      catch (error) {
        console.log(`Error when generating project (${name})`, error);
        // bar.stop();
        return false;
      }
    }

    if (!this._settings.noInstall) {
      // this._updateProgressBarOperation(bar, 'Install dependencies');
      console.log('Installing dependencies...');
      await this._installDependencies();
      // bar.increment();
    }

    for (const [ name, processor ] of this._postProcessors) {
      try {
        // this._updateProgressBarOperation(bar, name);
        await processor(this);
        // bar.increment();
      }
      catch (error) {
        // bar.stop();
        console.log('');
        console.log(`Error when generating project (${name})`, error);
        return false;
      }
    }

    if (!this._settings.noInstall) {
      // this._updateProgressBarOperation(bar, 'Install dependencies');
      await this._installDependencies();
    }

    // this._updateProgressBarOperation(bar, 'Done!');
    // bar.increment();
    // bar.stop();
    return true;
  }

  /**
   * Installs all registered dependencies.
   */
  private async _installDependencies(): Promise<void> {
    if (this._packages.size === 0) {
      return;
    }

    let packages = '';
    let devPackages = '';
    this._packages.forEach((isDevDependency, packageName) => {
      if (isDevDependency) {
        devPackages += ' ' + packageName;
      }
      else {
        packages += ' ' + packageName;
      }
    });

    // Install dependencies
    if (packages.length > 0) {
      await execSync(`npm i ${packages}`, this._settings.projectPath);
    }
    // Install dev dependencies
    if (devPackages.length > 0) {
      await execSync(`npm i --save-dev ${devPackages}`, this._settings.projectPath);
    }

    this._packages.clear();
  }

  private _updateProgressBarOperation(bar: cliProgress.SingleBar, operation: string) {
    bar.update({ operation: operation + ' | ' });
    bar.render();
  }

}
