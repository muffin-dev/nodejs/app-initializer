import { asBoolean } from '@muffin-dev/js-helpers';
import { writeFileAsync } from '@muffin-dev/node-helpers';
import { join } from 'path';
import { Generator } from '../generator';
import { updateJsonFile } from '../utilities';

const ESLINTRC_FILE_BASE = {
  env: {
    browser: true,
    es6: true,
    node: true,
  },
  extends: [
    'standard',
    'plugin:unicorn/recommended',
    '@muffin-dev/eslint-config',
  ],
  plugins: new Array<string>(),
  rules: { },
};

const ESLINTRC_FILE_NAME = '.eslintrc';
const ESLINTIGNORE_FILE_NAME = '.eslintignore';

module.exports = (generator: Generator) => {
  // Init values
  generator.settings.useESLint = asBoolean(generator.settings.useESLint);
  if (!generator.settings.useESLint) return;
  generator.settings.useTypescript = asBoolean(generator.settings.useTypescript);

  // Setup ESLint dependencies
  generator.addDependency('eslint', true);
  generator.addDependency('eslint-config-standard', true);
  generator.addDependency('eslint-plugin-import', true);
  generator.addDependency('eslint-plugin-node', true);
  generator.addDependency('eslint-plugin-promise', true);
  generator.addDependency('eslint-plugin-unicorn', true);
  generator.addDependency('@muffin-dev/eslint-config', true);

  // Add ESLint plugins for Typescript
  if (generator.settings.useTypescript) {
    generator.addDependency('@typescript-eslint/eslint-plugin', true);
    generator.addDependency('@typescript-eslint/parser', true);
  }

  // Add .eslintrc file
  generator.registerPreProcessor('Add .eslintrc file', async g => {
    const eslintrc = Object.assign({ }, ESLINTRC_FILE_BASE);

    // If the project to generate is an app
    if (g.settings.projectType === 'app') {
      eslintrc.env.browser = true;
    }

    // If user uses Typescript
    if (g.settings.useTypescript) {
      eslintrc.extends.push('plugin:@typescript-eslint/recommended');
      eslintrc.plugins.push('@typescript-eslint');
    }

    await writeFileAsync(join(g.outputPath, ESLINTRC_FILE_NAME), JSON.stringify(eslintrc, null, 2));
  });

  // Add .eslintignore file
  generator.registerPreProcessor('Add .eslintignore file', async g => {
    const eslintignore = new Array<string>();

    // If the project to generate is a package and user uses Typescript
    if (g.settings.projectType === 'package' && g.settings.useTypescript) {
      if (g.settings.packageSettings.hasBin) {
        eslintignore.push('/bin');
      }

      eslintignore.push('/doc');
      eslintignore.push('/lib');
    }

    // If user uses Typescript
    if (g.settings.useTypescript) {
      eslintignore.push('/index.js');
      eslintignore.push('/index.d.ts');
    }

    await writeFileAsync(join(g.outputPath, ESLINTIGNORE_FILE_NAME), eslintignore.join('\n'));
  });

  // Update package.json by adding lint scripts
  generator.registerPreProcessor('Add lint scripts', async g => {
    await updateJsonFile(join(g.outputPath, 'package.json'), packageJson => {
      packageJson.scripts.lint = `eslint ./**/*.${g.settings.useTypescript ? 'ts' : 'js'}`;
      packageJson.scripts.prepare = packageJson.scripts.prepare ? `npm run lint && ${packageJson.scripts.prepare}` : 'npm run lint';
      packageJson.scripts.prepublishOnly = packageJson.scripts.prepublishOnly ? `${packageJson.scripts.prepublishOnly} && npm run lint` : 'npm run lint';
    });
  });
};
