import { asBoolean } from '@muffin-dev/js-helpers';
import { removeDirectory } from '@muffin-dev/node-helpers';
import { join } from 'path';
import { Generator } from '../generator';
import { updateFile, updateJsonFile } from '../utilities';

module.exports = (generator: Generator) => {
  if (generator.settings.projectType !== 'package') return;
  generator.settings.packageSettings.hasBin = asBoolean(generator.settings.packageSettings.hasBin);

  // Remove /src/bin template if not needed
  if (!generator.settings.packageSettings.hasBin) {
    generator.registerPreProcessor('Remove /src/bin directory', async g => {
      try {
        await removeDirectory(join(g.outputPath, './src/bin'), true);
      }
      catch { }
    });
    return;
  }

  generator.addDependency('commander');

  generator.registerPreProcessor('Update package.json for executable support', async g => {
    await updateJsonFile(join(g.outputPath, 'package.json'), packageJson => {
      if (!packageJson.directories) {
        packageJson.directories = { };
      }
      if (!packageJson.files) {
        packageJson.files = [];
      }

      if (g.settings.packageSettings.hasBin) {
        packageJson.directories.bin = './bin';
        packageJson.files.push('bin/**/*');
      }

      // Set package bin executable
      if (g.settings.projectType === 'package' && asBoolean(g.settings.packageSettings.hasBin)) {
        if (!packageJson.bin) {
          packageJson.bin = { };
        }
        packageJson.bin[g.packageName] = `bin/index.${g.settings.useTypescript ? 'ts' : 'js'}`;
      }
    });
  });

  generator.registerPreProcessor('Update executable documentation', async g => {
    await updateFile(join(g.outputPath, `./src/bin/index.${g.settings.useTypescript ? 'ts' : 'js'}`), content => {
      return content.replace('#package-name#', g.packageName);
    });
  });
};
