import { join } from 'path';
import { Generator } from '../generator';
import { updateJsonFile } from '../utilities';

module.exports = (generator: Generator) => {
  if (!generator.settings.testFramework) return;

  if (generator.settings.testFramework === 'mocha') {
    generator.addDependency('mocha', true);

    generator.registerPreProcessor('Add mocha script in package.json', async g => {
      await updateJsonFile(join(g.outputPath, 'package.json'), packageJson => {
        packageJson.scripts.mocha = 'mocha';
        packageJson.scripts.test = 'npm run build && npm run mocha';
      });
    });
  }
};
