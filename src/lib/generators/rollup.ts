import { asBoolean } from '@muffin-dev/js-helpers';
import { readFileAsync, writeFileAsync } from '@muffin-dev/node-helpers';
import { join } from 'path';
import { Generator } from '../generator';
import { updateJsonFile } from '../utilities';

module.exports = (generator: Generator) => {
  generator.settings.packageSettings.useInBrowser = asBoolean(generator.settings.packageSettings.useInBrowser);
  if (generator.settings.projectType !== 'package' || !generator.settings.packageSettings.useInBrowser) {
    return;
  }

  generator.addDependency('rollup', true);
  generator.addDependency('rollup-plugin-terser', true);
  generator.addDependency('@rollup/plugin-commonjs', true);
  generator.addDependency('@rollup/plugin-node-resolve', true);

  // Setup rollup.config.js file
  generator.registerPreProcessor('Setup Rollup configuration', async g => {
    let rollupConfig = await readFileAsync(join(g.templatesDirectory, 'rollup.config.js.template'), 'utf8', true);
    if (!rollupConfig) return;

    rollupConfig = rollupConfig.replace('#package-name#', g.packageName);
    rollupConfig = rollupConfig.replace('#PackageName#', g.nicifiedPackageName);

    await writeFileAsync(join(g.outputPath, 'rollup.config.js'), rollupConfig);
  });

  // Setup rollup changes for package.json
  generator.registerPreProcessor('Setup rollup scripts in package.json', async g => {
    await updateJsonFile(join(g.outputPath, 'package.json'), packageJson => {
      packageJson.scripts.rollup = 'rollup -c';
      packageJson.scripts.build = packageJson.scripts.build
        ? `${packageJson.scripts.build} && npm run rollup`
        : 'npm run rollup';

      if (!packageJson.files) {
        packageJson.files = [];
      }
      packageJson.files.push(`${g.packageName}.min.js`);
    });
  });
};
