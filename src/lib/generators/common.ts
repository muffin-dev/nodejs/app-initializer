import { join } from 'path';
import { Generator } from '../generator';
import { execSync, isNullOrEmpty, updateFile, updateJsonFile } from '../utilities';
import sortPackageJson from 'sort-package-json';

module.exports = (generator: Generator) => {
  // Fix settings values
  if (isNullOrEmpty(generator.settings.projectName)) {
    generator.settings.projectName = `my-${generator.settings.projectType}`;
  }

  // Update package.json file
  generator.registerPreProcessor('Set basic infos in package.json file', async g => {
    await updateJsonFile(join(g.outputPath, 'package.json'), async packageJson => {
      // Set package name
      packageJson.name = g.settings.projectName.trim();
      // Set package description
      packageJson.description = !isNullOrEmpty(g.settings.description) ? g.settings.description : '';
      // Set package author
      packageJson.author = !isNullOrEmpty(g.settings.author) ? g.settings.author : '';

      // Set package keywords
      if (!Array.isArray(g.settings.keywords)) {
        if (!g.settings.keywords || typeof g.settings.keywords !== 'string') {
          g.settings.keywords = [];
        }
        else {
          const split = (g.settings.keywords as string).split(/\s*,\s*/);
          g.settings.keywords = new Array<string>();
          for (let s of split) {
            s = s.replace(/\\|"/gim, '');
            s = s.trim();
            if (s.length > 0) {
              g.settings.keywords.push(s);
            }
          }
        }
      }
      packageJson.keywords = g.settings.keywords;

      // Add engines for app projects
      if (g.settings.projectType === 'app') {
        packageJson.engines = { };
        try {
          const npmVersion = await execSync('npm -v');
          packageJson.engines.npm = npmVersion.trim();
        }
        catch { }

        try {
          const nodeVersion = await execSync('node -v');
          // "node -v" command outputs vXX.XX.XX, so it requires to remove the "v" prefix
          packageJson.engines.node = nodeVersion.trim().slice(1);
        }
        catch { }
      }
    });
  });

  // Udpate README.md file
  generator.registerPreProcessor('Set README file title', async g => {
    await updateFile(join(g.outputPath, 'README.md'), readme => {
      const readmeLines = readme.split(/\r?\n/);
      readmeLines[0] = `# ${g.nicifiedPackageName}`;
      return readmeLines.join('\n');
    });
  });

  // Sort package.json keys
  generator.registerPostProcessor('Sort package.json keys', async g => {
    await updateFile(join(g.outputPath, 'package.json'), packageJsonContent => {
      let packageJson = JSON.parse(packageJsonContent);
      packageJson = sortPackageJson(packageJson);
      return JSON.stringify(packageJson, null, 2);
    });
  });
};
