import { writeFileAsync } from '@muffin-dev/node-helpers';
import axios from 'axios';
import { join } from 'path';
import { Generator } from '../generator';
import { execSync, isNullOrEmpty, updateFile, updateJsonFile } from '../utilities';

module.exports = (generator: Generator) => {
  if (isNullOrEmpty(generator.settings.git)) return;
  if (!generator.settings.git.endsWith('.git')) {
    generator.settings.git += '.git';
  }

  // Copy Node.js gitignore template from GitHub
  generator.registerPreProcessor('Copy Node.js gitignore template from GitHub', async g => {
    try {
      const response = await axios.get('https://api.github.com/gitignore/templates/Node');
      await writeFileAsync(join(g.outputPath, '.gitignore'), response.data.source);
    }
    catch { }

    await updateFile(join(g.outputPath, '.gitignore'), gitignore => {
      const gitignoreLines = gitignore.split(/\r?\n/);
      gitignoreLines.push('');

      if (g.settings.projectType === 'package') {
        if (!g.settings.packageSettings || g.settings.packageSettings.hasBin) {
          gitignoreLines.push('/bin');
        }

        gitignoreLines.push('/doc');
        gitignoreLines.push('/lib');
      }
      else if (g.settings.projectType === 'app') {
        gitignoreLines.push('/dist');
      }

      if (g.settings.useTypescript) {
        gitignoreLines.push('/test');
        gitignoreLines.push('/tests');
        gitignoreLines.push('index.js');
        gitignoreLines.push('index.d.ts');
        gitignoreLines.push('index.d.ts.map');
      }

      return gitignoreLines.join('\n');
    });
  });

  // Init Git repository if possible
  generator.registerPreProcessor('Initialize Git local repository', async g => {
    let isGitInstalled = false;
    // Check git install
    try {
      execSync('git --version');
      isGitInstalled = true;
    }
    catch {
      // console.warn('No Git install detected, the repository won\'t be initialized. Go to https://git-scm.com to get the Git installer.');
    }

    if (isGitInstalled) {
      await execSync('git init', g.outputPath);
      await execSync(`git remote add origin ${g.settings.git}`, g.outputPath);
      await execSync('git add .', g.outputPath);
      await execSync('git commit -m "Init project"', g.outputPath);
    }
  });

  // Update package.json
  generator.registerPreProcessor('Set Git repository informations in package.json', async g => {
    await updateJsonFile(join(g.outputPath, 'package.json'), packageJson => {
      packageJson.repository = {
        type: 'git',
        url: `git+${g.settings.git}`
      };

      packageJson.homepage = `${g.settings.git.slice(0, g.settings.git.length - '.git'.length)}#readme`;
      packageJson.bugs = {
        url: `${g.settings.git.slice(0, g.settings.git.length - '.git'.length)}/issues`
      };
    });
  });
};
