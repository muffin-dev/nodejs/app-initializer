import { copyDirectory } from '@muffin-dev/node-helpers';
import { join } from 'path';
import { Generator } from '../generator';
import { updateJsonFile } from '../utilities';

module.exports = (generator: Generator) => {
  if (generator.settings.projectType !== 'package') return;

  generator.addDependency('@muffin-dev/node-helpers', true);

  generator.registerPreProcessor('Copy package template files', async g => {
    const templateDir = join(g.templatesDirectory, `package-${g.settings.useTypescript ? 'ts' : 'js'}`);
    await copyDirectory(templateDir, join(g.outputPath));
  });

  generator.registerPreProcessor('Setup included files and directories in package.json', async g => {
    await updateJsonFile(join(g.outputPath, 'package.json'), packageJson => {
      if (!packageJson.directories) {
        packageJson.directories = { };
      }
      if (!packageJson.files) {
        packageJson.files = [];
      }

      packageJson.directories.lib = './lib';
      packageJson.directories.doc = './doc';

      packageJson.files.push('lib/**/*');
      packageJson.files.push('doc/**/*');
      packageJson.files.push('index.js');
    });
  });
};
