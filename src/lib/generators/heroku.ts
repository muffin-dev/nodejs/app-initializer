import { asBoolean } from '@muffin-dev/js-helpers';
import { writeFileAsync } from '@muffin-dev/node-helpers';
import { join } from 'path';
import { Generator } from '../generator';
import { updateJsonFile } from '../utilities';

module.exports = (generator: Generator) => {
  if (generator.settings.projectType !== 'app' || generator.settings.appSettings.deployPlatform !== 'heroku') {
    return false;
  }
  generator.settings.appSettings.isWorker = asBoolean(generator.settings.appSettings.isWorker);

  generator.registerPreProcessor('Add Procfile', async g => {
    const procfile = generator.settings.appSettings.isWorker ? 'worker: node index.js' : 'web: node dist/server.js';
    await writeFileAsync(join(g.outputPath, 'Procfile'), procfile);
  });

  generator.registerPreProcessor('Add Heroku deploy script', async g => {
    await updateJsonFile(join(g.outputPath, 'package.json'), packageJson => {
      packageJson.scripts.deploy = 'git push heroku master';
    });
  });
};
