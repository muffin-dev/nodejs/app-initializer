import { asBoolean } from '@muffin-dev/js-helpers';
import { join } from 'path';
import { Generator } from '../generator';
import { execSync, updateFile, updateJsonFile } from '../utilities';

const TSCONFIG_SETTINGS = new Map<string, any>();
TSCONFIG_SETTINGS.set('declaration', true);
TSCONFIG_SETTINGS.set('outDir', './'); // Changed at runtime depending on the project type
TSCONFIG_SETTINGS.set('downlevelIteration', true);
TSCONFIG_SETTINGS.set('removeComments', true);
TSCONFIG_SETTINGS.set('strictNullChecks', false);
TSCONFIG_SETTINGS.set('experimentalDecorators', true);

module.exports = (generator: Generator) => {
  generator.settings.useTypescript = asBoolean(generator.settings.useTypescript);
  generator.settings.noInstall = asBoolean(generator.settings.noInstall);
  if (!generator.settings.useTypescript) return;

  TSCONFIG_SETTINGS.set('outDir', generator.settings.projectType === 'app' ? './dist' : './');

  // Register dependencies
  generator.addDependency('typescript', true);
  generator.addDependency('@types/node', true);
  generator.addDependency('ts-node', true);

  // Update package.json
  generator.registerPreProcessor('Setup typescript build commands', async g => {
    await updateJsonFile(join(g.outputPath, 'package.json'), packageJson => {
      if (g.settings.projectType === 'package') {
        if (!packageJson.files) {
          packageJson.files = [];
        }
        packageJson.files.push('index.d.ts');
      }

      packageJson.scripts.tsc = 'tsc';
      packageJson.scripts['ts-node'] = 'ts-node';
      packageJson.scripts.build = packageJson.scripts.build ? `${packageJson.scripts.build} && tsc` : 'tsc';
    });
  });

  // Init tsconfig.json file
  generator.registerPostProcessor('Initialize tsconfig.json', async g => {
    if (generator.settings.noInstall) {
      await execSync('npx tsc --init', g.outputPath);
    }
    else {
      await execSync('npm run tsc -- --init', g.outputPath);
    }

    // Enable custom configuration lines
    await updateFile(join(g.outputPath, 'tsconfig.json'), tsconfig => {
      const outputLines = new Array<string>();
      const tsconfigLines = tsconfig.split(/\r?\n/);
      for (let l of tsconfigLines) {
        // If the current line starts by a comment (a disabled option)
        if (l.match(/\/{2,}/)) {
          let hasCustomProperty = false;
          // If the line contains a custom property, enable it
          TSCONFIG_SETTINGS.forEach((value, key) => {
            if (l.includes(key + '":')) {
              l = l.replace(/\/{2,}\s*/, '');
              hasCustomProperty = true;
            }
          });

          // If the line doesn't contain a custom property, just remove it from the tsconfig.json file
          if (!hasCustomProperty) {
            continue;
          }
        }

        // Clear any line that contain a multiline comment
        l = l.replace(/\/\*.+\*\//gim, '');
        if (l.trim().length > 0) {
          outputLines.push(l);
        }
      }

      return outputLines.join('\n');
    });

    // Update custom configuration values
    await updateJsonFile(join(g.outputPath, 'tsconfig.json'), tsconfig => {
      if (!tsconfig.compilerOptions) {
        tsconfig.compilerOptions = { };
      }

      TSCONFIG_SETTINGS.forEach((value, key) => {
        tsconfig.compilerOptions[key] = value;
      });

      tsconfig.include = [ 'src' ];
      tsconfig.exclude = [ 'node_modules' ];
    });
  });
};
