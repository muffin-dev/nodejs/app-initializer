import { asBoolean } from '@muffin-dev/js-helpers';
import { join } from 'path';
import { Generator } from '../generator';
import { updateJsonFile } from '../utilities';

module.exports = (generator: Generator) => {
  generator.settings.isPublic = asBoolean(generator.settings.isPublic);
  if (!generator.settings.isPublic) return;

  generator.registerPreProcessor('Setup publishConfig key in package.json', async g => {
    await updateJsonFile(join(g.outputPath, 'package.json'), packageJson => {
      if (!packageJson.publishConfig) {
        packageJson.publishConfig = { };
      }
      packageJson.publishConfig.access = 'public';
    });
  });
};
