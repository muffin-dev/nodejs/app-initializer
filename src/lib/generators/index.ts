import { Generator } from '../generator';

module.exports = (generator: Generator) => {
  // Setup project-specific content
  require('./app')(generator);
  require('./package')(generator);

  // Setup project-independent common content
  require('./common')(generator);
  require('./publish-config')(generator);

  // Setup project depending on other settings
  require('./bin')(generator);
  require('./eslint')(generator);
  require('./heroku')(generator);
  require('./surge')(generator);
  require('./typescript')(generator);
  require('./rollup')(generator);
  require('./git')(generator);
};
