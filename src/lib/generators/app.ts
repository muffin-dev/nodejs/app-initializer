import { copyDirectory } from '@muffin-dev/node-helpers';
import { join } from 'path';
import { Generator } from '../generator';

module.exports = (generator: Generator) => {
  if (generator.settings.projectType !== 'app') return;

  generator.addDependency('express');
  generator.addDependency('@muffin-dev/node-helpers', true);

  generator.registerPreProcessor('Copy app template files', async g => {
    const templateDir = join(g.templatesDirectory, `app-${g.settings.useTypescript ? 'ts' : 'js'}`);
    await copyDirectory(templateDir, join(g.outputPath));
  });
};
