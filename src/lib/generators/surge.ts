import { writeFileAsync } from '@muffin-dev/node-helpers';
import { join } from 'path';
import { Generator } from '../generator';
import { isNullOrEmpty, updateJsonFile } from '../utilities';

module.exports = (generator: Generator) => {
  if (generator.settings.projectType !== 'app' || generator.settings.appSettings.deployPlatform !== 'surge') {
    return false;
  }

  if (isNullOrEmpty(generator.settings.appSettings.surgeAppDomain)) {
    generator.settings.appSettings.surgeAppDomain = '';
  }

  generator.addDependency('surge');

  generator.registerPreProcessor('Add CNAME file', async g => {
    if (g.settings.appSettings.surgeAppDomain.length > 0) {
      await writeFileAsync(join(g.outputPath, 'CNAME'), g.settings.appSettings.surgeAppDomain);
    }
  });

  generator.registerPreProcessor('Add Surge deployment script', async g => {
    await updateJsonFile(join(g.outputPath, 'package.json'), packageJson => {
      packageJson.scripts.deploy = `npm run build && npm run surge -- ./ ${g.settings.appSettings.surgeAppDomain}`;
      packageJson.scripts.surge = 'surge';
    });
  });
};
