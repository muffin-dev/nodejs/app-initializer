import { generateProject } from './lib';

export * from './lib/index';

export default generateProject;
